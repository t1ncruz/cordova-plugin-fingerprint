# Finger Print cordova plugin (Based on OutSystems Touch ID Plugin https://github.com/OutSystems/cordova-plugin-fingerprint.git#2.0.1)

  Cordova Plugin to leverage the local authentication framework to allow in-app user authentication using Touch ID.

## Supported Platforms

  - iOS
  - Android

## Dependencies 

  - cordova-plugin-touchid (iOS)
  - cordova-plugin-fingerprint-auth (Android)